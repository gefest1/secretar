import React, { useEffect, useState } from "react";
import "@fullcalendar/common/main.css";
import "@fullcalendar/timegrid/main.css";
import FullCalendar, { EventContentArg } from "@fullcalendar/react";
import timeGridPlugin from "@fullcalendar/timegrid";
import styled from "styled-components";
import Modal from "components/atoms/Modal";
import { useRecoilState } from "recoil";
import {
    addDays,
    addMinutes,
    format,
    getHours,
    Locale,
    subDays,
    subYears,
} from "date-fns";
import ru from "date-fns/locale/ru";
import { useRouter } from "next/router";
import { doctorEventsState } from "@recoil/atoms/doctorEvents";
import {
    GET_BOOKINGS_OF_DOCTOR,
    GET_UPCOMING_QUERIES,
} from "src/api/queries/bookings";
import { CalendarEvent, getTemplate } from "@core/lib/events";
import { PatientEntity } from "@core/types/patient/IPatient";
import { Booking, BookingProgress } from "@core/types/bookings/IBookings";
import Link from "next/link";
import { startSessionMutation } from "@src/api/mutations/session";
import { getDoctorTokens } from "@src/utils/getToken";
import { useQuery } from "@apollo/client";
import { cancelBooking } from "@src/api/mutations/cancel-bookings";
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import interactionPlugin from "@fullcalendar/interaction";

import {
    getBookingsOfDoctor,
    getBookingsOfDoctorVariables,
} from "@graphqlTypes/getBookingsOfDoctor";
import { GET_UPCOMING_BOOKINGS } from "@src/api/queries/getUpcomingBookings";
import AppointmentModal from "components/atoms/AppointmentModal";
import { useAppointment } from "@recoil/hooks/useAppointment";
import { GET_DOCTORS_SEARCH } from "@src/api/queries/getDoctorSearch";
import { GetDoctorSearch } from "@graphqlTypes/GetDoctorSearch";
import { getUpcomingBookingsOfDoctor } from "@graphqlTypes/getUpcomingBookingsOfDoctor";
import { GetAppointmentBlanksOfUserVariables } from "@graphqlTypes/GetAppointmentBlanksOfUser";
import {
    GetBookingsByDate,
    GetBookingsByDateVariables,
} from "@graphqlTypes/GetBookingsByDate";
import { addMonths } from "date-fns";

const AdminPage: React.FC = () => {
    const { token, doctorId } = getDoctorTokens();
    const [showPatientDetailsModal, setShowPatientDetailsModal] =
        useState(false);
    const [eventDetailsModalData, setEventDetailsModalData] =
        useState<EventDataDTO | null>(null);
    const [today, setToday] = useState(new Date());
    const [events, setEvents] = useRecoilState(doctorEventsState);
    const router = useRouter();
    const [appointmentData, { setAppointmentDoctor, setTime, setShow }] =
        useAppointment();

    const { data: bookingsRes, loading: bookingsLoading } = useQuery<
        GetBookingsByDate,
        GetBookingsByDateVariables
    >(GET_UPCOMING_BOOKINGS, {
        variables: {
            firstDate: subYears(today, 1),

            secondDate: addMonths(today, 1),
        },
        context: {
            // example of setting the headers with context per operation
            headers: {
                Authorization: token,
            },
        },
    });
    const { data, loading: patientLoading } =
        useQuery<GetDoctorSearch>(GET_DOCTORS_SEARCH);
    const bookings = bookingsRes?.getBookingsByDate;
    const onStartSession = async (event: any) => {
        await startSessionMutation(
            { bookingId: event.bookingId },
            { token: token },
        );
        setShowPatientDetailsModal(false);
    };
    const doctors = data?.getAllDoctors;
    useEffect(() => {
        if (events.length === 0) {
            if (bookings && !bookingsLoading)
                setEvents(convertBookingToEvents(bookings));
        }
        setAppointmentDoctor(null);
    }, [bookings, bookingsLoading, patientLoading, doctors]);
    useEffect(() => {
        if (bookings && !bookingsLoading) {
            if (!appointmentData.doctor) {
                return setEvents(convertBookingToEvents(bookings));
            }
            const curDoctorId = appointmentData?.doctor?._id;

            setEvents(
                convertBookingToEvents(
                    bookings.filter((b) => {
                        return b.doctor._id === curDoctorId;
                    }),
                ),
            );
        }
    }, [appointmentData.doctor]);

    if (bookingsLoading || patientLoading) {
        return <div>Loading</div>;
    }
    return (
        <div>
            <div className="flex justify-between items-center">
                <div className=" flex flex-col">
                    <h1 className="text-4xl font-bold capitalize">
                        Расписание
                    </h1>
                    <div className=" form-control">
                        <label className=" label">Выберите доктора</label>
                        <select
                            onChange={(e) => {
                                if (e.target.value === "") {
                                    return setAppointmentDoctor(null);
                                }
                                const selectedIndex = e.target.value;

                                setAppointmentDoctor(doctors[selectedIndex]);
                                console.log(doctors[selectedIndex]);
                            }}
                            className="select select-bordered"
                            id="doctors"
                        >
                            <option value="">Все</option>
                            {doctors?.map((doctor, index) => (
                                <option value={index}>{doctor.fullName}</option>
                            ))}
                        </select>
                    </div>
                </div>
                {/* <div className="">
                    <button className="mr-4">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-8 w-8"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                            />
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                            />
                        </svg>
                    </button>
                    <button className=" text-purple-600">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-8 w-8"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </button>
                </div> */}
            </div>
            <div>
                <FullCalendar
                    plugins={[timeGridPlugin]}
                    initialView="timeGridWeek"
                    nowIndicator
                    dayHeaderClassNames="font-light text-left text-dark-grey w-full"
                    slotLabelClassNames="font-light text-left text-dark-grey w-full"
                    locale={{ code: "ru" }}
                    editable={true}
                    eventMinHeight={100}
                    allDaySlot={false}
                    titleFormat={{ month: "short", day: "numeric" }}
                    headerToolbar={{
                        start: "prev", // will normally be on the left. if RTL, will be on the right
                        center: "title",
                        end: "next", // will normally be on the right. if RTL, will be on the left
                    }}
                    expandRows={true}
                    eventTimeFormat={{
                        hour: "2-digit",
                        minute: "2-digit",
                        meridiem: true,
                    }}
                    slotLabelFormat={{
                        hour: "2-digit",
                        minute: "2-digit",
                        omitZeroMinute: false,
                        meridiem: "short",
                    }}
                    slotMinTime={{
                        hours: appointmentData?.doctor
                            ? getHours(
                                  new Date(
                                      appointmentData?.doctor?.workTimes?.[0]?.startTime,
                                  ),
                              )
                            : 6,
                    }}
                    slotMaxTime={{
                        hours: appointmentData?.doctor
                            ? getHours(
                                  new Date(
                                      appointmentData?.doctor?.workTimes?.[0]?.endTime,
                                  ),
                              )
                            : 21,
                    }}
                    slotDuration={{ minutes: 30 }}
                    eventContent={renderEventContent}
                    eventClick={({ event }) => {
                        if (event.extendedProps.type === "On") {
                            return router.push(
                                `/patients/${event.extendedProps.patient._id}/home`,
                            );
                        }
                        if (event.extendedProps.type !== "Upcoming") return;
                        setShowPatientDetailsModal(true);
                        setEventDetailsModalData({
                            patient: event?.extendedProps
                                ?.patient as PatientEntity,
                            startDate: event?.start ?? new Date(),
                            title: event.title,
                            bookingId: event?.extendedProps?.booking?._id,
                        });
                    }}
                    events={events}
                />
            </div>
            {eventDetailsModalData && (
                <AppointmentDetailModal
                    active={showPatientDetailsModal}
                    eventData={eventDetailsModalData}
                    onClose={() => setShowPatientDetailsModal(false)}
                    onSessionStart={(event) => {
                        onStartSession(event);
                        setShowPatientDetailsModal(false);
                    }}
                    onSessionCancel={async (event) => {
                        await cancelBooking(event.bookingId, token);
                        alert("Запись отменена");
                        setShowPatientDetailsModal(false);
                    }}
                ></AppointmentDetailModal>
            )}
            <AppointmentModal></AppointmentModal>
        </div>
    );
};

const convertBookingToEvents = (bookings: Booking[]) => {
    const calendarEvents = bookings.map(
        (booking) => new CalendarEvent(booking),
    );
    return calendarEvents.map(({ title, start, ...extendedProps }) => {
        return {
            title,
            backgroundColor: "transparent",
            borderColor: "transparent",
            start,

            extendedProps: { ...extendedProps },
        };
    });
};
interface EventDataDTO {
    title: string;
    startDate: Date;
    patient: PatientEntity;
    bookingId: string;
}
interface AppointmentDetailModalProps {
    active: boolean;
    eventData: EventDataDTO;
    onClose: () => void;
    onSessionStart: (eventData: EventDataDTO) => void;
    onSessionCancel: (eventData: EventDataDTO) => void;
}

const AppointmentDetailModal: React.FC<AppointmentDetailModalProps> = ({
    active,
    eventData,
    onClose,
    onSessionStart,
    onSessionCancel,
}) => {
    return (
        <Modal active={active}>
            <div className="w-2/5 bg-light-grey relative p-6 rounded-lg">
                <button
                    onClick={onClose}
                    className="btn btn-ghost p-0 absolute top-0 right-0 m-4"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M6 18L18 6M6 6l12 12"
                        />
                    </svg>
                </button>
                <div>
                    <p>
                        {format(eventData.startDate, "dd.MM.yyyy hh:mm", {
                            locale: ru,
                        })}
                    </p>
                    <div className=" shadow-xl w-full flex items-center justify-between p-4 rounded-md mb-4">
                        <div className="flex items-center">
                            <div className="flex flex-col items-start">
                                <p className="font-medium text-xl">
                                    {eventData.patient.fullName}
                                </p>
                                <p>{eventData.patient.phoneNumber}</p>
                            </div>
                        </div>
                    </div>
                    <div className="grid grid-cols-2 gap-4">
                        <Link href={`/patients/${eventData.patient._id}/home`}>
                            <div
                                onClick={() => {
                                    console.log(eventData);
                                    onSessionStart(eventData);
                                }}
                                className="btn btn-primary py-2 text-lg text-center"
                            >
                                Перейти к пациенту
                            </div>
                        </Link>
                        <button
                            onClick={() => onSessionCancel(eventData)}
                            className="btn btn-outline btn-primary py-2 text-lg"
                        >
                            Отменить прием
                        </button>
                    </div>
                </div>
            </div>
        </Modal>
    );
};

const renderEventContent = (eventInfo: EventContentArg) => {
    const { event } = eventInfo;
    const { type, patient, service } = event.extendedProps as {
        type: BookingProgress;
        patient: PatientEntity;
        service: any;
    };
    const eventTemplate = getTemplate(type) ?? null;
    return (
        eventTemplate && (
            <EventContainer
                {...eventTemplate.eventContainer}
                className="w-full h-full p-2 rounded flex flex-col justify-start    "
            >
                <div className="flex items-center justify-start mb-2">
                    <p className="text-sm truncate text-current  ">
                        {patient?.fullName}
                    </p>
                </div>
                <EventSubtitle service={service} type={type} />
            </EventContainer>
        )
    );
};

interface EventSubtitleProps {
    type: BookingProgress;
    service: any;
}

const EventSubtitle: React.FC<EventSubtitleProps> = ({ type, service }) => {
    const subtitle = getTemplate(type)?.subtitle;
    return (
        <div className="flex items-center justify-start">
            {subtitle?.icon && (
                <div>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="w-6 text-error mr-2"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        {subtitle?.icon}
                    </svg>
                </div>
            )}
            <div className="flex flex-col gap-2">
                {subtitle?.label}
                {service?.name}
            </div>
        </div>
    );
};

interface EventContainerProps {
    backgroundColor: string;
    accentColor: string;
    contentColor: string;
}

const EventContainer = styled.div<EventContainerProps>`
    background-color: ${({ backgroundColor }) => backgroundColor};
    border-left: solid 12px ${({ accentColor }) => accentColor};
    color: ${({ contentColor }) => contentColor};
`;

export default AdminPage;
