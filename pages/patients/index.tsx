import getAdminLayout from "components/layouts/adminLayout";
import React, { useEffect, useState } from "react";
import { ElevatedContainer } from "components/atoms/ElevatedContainer";
import { PatientEntity } from "@core/types/patient/IPatient";
import Link from "next/link";
import { useRecoilState } from "recoil";
import { patientsState } from "@recoil/atoms/patients";
import gql from "graphql-tag";
import client from "src/apollo/apollo-client";

const PatientsPage: React.FC<{ patients: PatientEntity[] }> = ({
    patients,
}) => {
    const [searchName, setSearchName] = useState("");
    let filteredPatiens = patients;
    const re = new RegExp(searchName + ".+$", "i");
    filteredPatiens = filteredPatiens?.filter(function (e, i, a) {
        return e.fullName.search(re) != -1;
    });
    return (
        <div className="">
            <div className="flex justify-between">
                <h1 className="text-4xl font-bold mb-4">Пациенты</h1>
                <div className=" flex   mb-4">
                    <input
                        onChange={(e) => setSearchName(e.target.value)}
                        type="text"
                        value={searchName}
                        placeholder="Поиск"
                        className="input w-64 bg-base-200 mr-4"
                    />
                    <Link href="/patients/add-patient">
                        <button className="btn btn-primary">
                            Добавить пациента
                        </button>
                    </Link>
                </div>
            </div>
            <div className="w-full border-b-2 border-base-200 mb-4"></div>
            <div className="grid grid-cols-3 gap-4">
                {filteredPatiens.map((patient) => (
                    <PatientCard
                        key={patient?._id}
                        patient={patient}
                    ></PatientCard>
                ))}
            </div>
        </div>
    );
};

const PatientCard: React.FC<{ patient: PatientEntity }> = ({ patient }) => {
    return (
        <ElevatedContainer className="rounded-lg p-4 h-full">
            <div className="flex items-center ">
                <Link href={`/patients/${patient?._id}/home`}>
                    <div className=" cursor-pointer avatar mr-4">
                        <div className="rounded-full w-20 h-20 bg-primary">
                            <span className="text-4xl text-white uppercase flex items-center justify-center h-full">
                                {patient.fullName[0]}
                            </span>
                        </div>
                    </div>
                </Link>

                <div className="flex flex-col justify-start">
                    <Link href={`/patients/${patient?._id}/home`}>
                        <span className=" cursor-pointer text-2xl font-medium w-1/2 h-16">
                            {patient.fullName}
                        </span>
                    </Link>

                    <a
                        href={`tel: ${patient.phoneNumber}`}
                        className="text-base-300"
                    >
                        Телефон: +{patient.phoneNumber}
                    </a>
                </div>
            </div>
        </ElevatedContainer>
    );
};

export async function getServerSideProps() {
    const PATIENTS_LIST = gql`
        query {
            listUsers {
                fullName
                _id
                phoneNumber
                photoURL {
                    m
                }
            }
        }
    `;
    const { data } = await client.query({
        query: PATIENTS_LIST,
    });
    const patients = data?.listUsers;

    return {
        props: { patients },
        // will be passed to the page component as props
    };
}
export default PatientsPage;
